/**
 * Created by Igor on 15/09/2015.
 */
L.Control.NorthArrow = L.Control.extend({
    options: {
        position: 'bottomleft'
    },
    onAdd: function () {
    var div = L.DomUtil.create("div","info");
        div.innerHTML = '<img class=north-arrow src="/styles/images/north-arrow.png">';
    return div;
    }
});
L.control.northarrow = function (options) {
    return new L.Control.NorthArrow(options);
};